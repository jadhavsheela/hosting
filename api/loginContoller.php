<?php


if(!isset($_POST['useremail'])){


}else{
	require '../database/db.php';

    $obj = new DB();
    $con = $obj->db_connect();

    if(!$con){
		echo 'Something went Wrong';
	}else{
		
        $useremail = $_POST['useremail'];
        $userpassword = $_POST['userpassword'];

        $sql = ("SELECT id, first_name, userpassword, is_verified FROM users WHERE email=?;");
        $stmt = mysqli_stmt_init($con);
        if(!mysqli_stmt_prepare($stmt, $sql)){
            echo "SQL statement failed";
        }else{
            mysqli_stmt_bind_param($stmt,"s",$useremail);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            $row = mysqli_fetch_assoc($result);
            
            if($row['is_verified']){
                if(password_verify($userpassword, $row['userpassword'])){
                    session_start();
                    $_SESSION['emailValid'] = $useremail;
                    $_SESSION['userID'] = $row['id'];
                    $_SESSION['firstName'] =$row['first_name'];
                    echo 'ok';
                }
            }

        }

    }

}

?>