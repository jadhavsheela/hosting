<?php

include_once '../database/db.php';

$obj = new DB();
$con = $obj->db_connect();

if(!$con){
    echo 404;
}else{
    $cname = $_POST['cName'];
    $cemail = $_POST['cEmail'];
    $csubject = $_POST['cSubject'];
    $cmessgae = $_POST['cMessage'];
   
    
    $sql = ('insert into contactus(cust_email,cust_name,cust_subject,cust_message) values(?,?,?,?);');
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt, $sql)){
        echo 'Query Failed';
    }else{
        mysqli_stmt_bind_param($stmt, "ssss", $cemail, $cname, $csubject, $cmessgae);
        mysqli_stmt_execute($stmt);
        echo "Success";
    }
}
?>