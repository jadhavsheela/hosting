<?php


class DB
{

    static $host = 'localhost';
    static $user = 'root';
    // static $pass = 'Deep@1920$deep&';
    static $pass = '';
    static $db = 'db_eurotech';
    static $con;

    public function db_connect()
    {

        self::$con = mysqli_connect(self::$host, self::$user, self::$pass, self::$db);

        if (!self::$con) {
            echo "Unable to Connect, Debugging Error: " . mysqli_connect_errno() . ", " . mysqli_connect_error();
            return NULL;
        } else {
            return self::$con;
            echo "DB Connect Successfully";
        }
    }

    
}
