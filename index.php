<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Header Includes  -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>EuroTech Paints</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/footer.css">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- Preloader -->
  <link rel="stylesheet" href="assets/css/preloader.css">
<!-- Google Anaytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-180168407-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-180168407-1');
</script>

</head>

<body>
<style>
  .text{
    font-size:14px;
  }
</style>
  
  
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:eurotechpaints.solapur@gmail.com">eurotechpaints.solapur@gmail.com</a>
        <i class="icofont-phone"></i><a href="tel:+919370722875">+91 9370 722 875</a>
      </div>
      <div class="social-links float-right">
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="https://www.instagram.com/euro_tech_paints/" target="_blank" class="instagram"><i class="icofont-instagram"></i></a>
      </div>
    </div>
  </section>
 
 
 <!-- ======= Header ======= -->
 <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>Mamba</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="index.php"><img src=" assets/img/logo1.png" alt="eurotech_logo" class="img-fluid"><span class="text" style=" margin-left: 25px; margin-top: 10px; font-size: 25px;"><b>EURO TECH PAINTS</b></span></a></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="index.php#about">About Us</a></li>
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#products">Products</a></li>
          <!-- <li><a href="#team">Team</a></li> -->
          <!-- <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li> -->
          <li><a href="#contact">Contact Us</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background-image: url(' assets/img/slide/slide (1).jfif');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">EuroTech Paints</h2>
                <p class="animate__animated animate__fadeInUp">Bringing your home back to life with quality painting.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">AboutUs</a>
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background-image: url(' assets/img/slide/slide (2).jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">QUALITY ASSURANCE</h2>
                <p class="animate__animated animate__fadeInUp">Paragraph</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <div class="carousel-item" style="background-image: url('assets/img/25.PNG');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown"></h2>
                <p class="animate__animated animate__fadeInUp"></p>
                <!-- <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a> -->
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row no-gutters">
          <div class="col-lg-6 video-box">
            <img src=" assets/img/about-us.jpg" class="img-fluid" alt="about-us">
            <a href="https://www.youtube.com/watch?reload=9&v=8xhi85pykFo" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center about-content">

            <div class="section-title">
              <h2>About Us</h2>
              <p>PThe company comes as a major Manufacturer and Supplier of a large collection of All Types Of Decorative Paints which are utilized for many residential and commercial purposes. The company has come this far taking into view the sophisticated efforts of Director Mr.Pramod Dubey. PARAS COLOURS AND PIGMENTS has expertise when it comes to offering a large range of high-quality proven colours. Our assortment of products come very much inclusive of Luxurious High Gloss Emulsion Interior & Exterior Emulsion, Water Based luster, Acrylic Washable Distemper, Redoxide Primer (Water-Based), Water Thinnable Primer Interior & Exterior, Wall Putty Acrylic & cement-based, Waterproof Cement paint and all types of Waterbased Decorative Paints.</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Quality</a></h4>
              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-comment-check"></i></div>
              <h4 class="title"><a href="">Trust</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->



    <!-- ======= Counts Section ======= -->
    <section class="counts section-bg">
      <div class="container">

        <div class="row">

          <div class="col-lg-3 col-md-6 text-center" data-aos="fade-up">
            <div class="count-box">
              <i class="icofont-simple-smile" style="color: #20b38e;"></i>
              <span data-toggle="counter-up">2500</span>
              <p>Happy Clients</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 text-center" data-aos="fade-up" data-aos-delay="200">
            <div class="count-box">
              <i class="icofont-document-folder" style="color: #c042ff;"></i>
              <span data-toggle="counter-up">50</span>
              <p>Products</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 text-center" data-aos="fade-up" data-aos-delay="400">
            <div class="count-box">
              <i class="icofont-live-support" style="color: #46d1ff;"></i>
              <span data-toggle="counter-up">24</span>
              <p>Hours Of Support</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 text-center" data-aos="fade-up" data-aos-delay="600">
            <div class="count-box">
              <i class="icofont-users-alt-5" style="color: #ffb459;"></i>
              <span data-toggle="counter-up">15</span>
              <p>Hard Workers</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Counts Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Services</h2>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up">
            <div class="icon"><i class="icofont-computer"></i></div>
            <h4 class="title"><a href="">Lorem Ipsum</a></h4>
            <p class="description"></p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
            <div class="icon"><i class="icofont-chart-bar-graph"></i></div>
            <h4 class="title"><a href="">Dolor Sitema</a></h4>
            <p class="description"></p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
            <div class="icon"><i class="icofont-earth"></i></div>
            <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
            <p class="description"></p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
            <div class="icon"><i class="icofont-image"></i></div>
            <h4 class="title"><a href="">Magni Dolores</a></h4>
            <p class="description"></p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
            <div class="icon"><i class="icofont-settings"></i></div>
            <h4 class="title"><a href="">Nemo Enim</a></h4>
            <p class="description"></p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
            <div class="icon"><i class="icofont-tasks-alt"></i></div>
            <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
            <p class="description"></p>
          </div>
        </div>

      </div>
    </section>

    <!-- ======= Our Portfolio Section ======= -->
    <section id="products" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>Our Products</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-Primeres">Primeres</li>
              <li data-filter=".filter-Stainers">Stainers</li>
              <li data-filter=".filter-Others">Others</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-Primeres">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-6 portfolio-item filter-Primeres">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-6 portfolio-item filter-Others">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-6 portfolio-item filter-Others">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-6 portfolio-item filter-Stainers">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-6 portfolio-item filter-Stainers">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-6 portfolio-item filter-Stainers">
            <div class="portfolio-wrap">
              <img src=" assets/img/1.JPG" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Product Category</h4>
                <p>Name of Product</p>
                <div class="portfolio-links">
                  <a href=" assets/img/1.JPG" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="products-details.php" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>

         

        </div>

      </div>
    </section><!-- End Our Portfolio Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Why Us</h2>
        </div>

        <div class="row  d-flex align-items-stretch">

          <div class="col-lg-6 faq-item" data-aos="fade-up">
          
          </div>

          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="100">
          
          </div>

          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="200">
           
          </div>

          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="300">
          
          </div>

          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="400">
          
          </div>

          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="500">
         
          </div>

        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Us Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Contact Us</h2>
        </div>

        <div class="row">

          <div class="col-lg-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="info-box">
              <i class="bx bx-map"></i>
              <h3>Our Address</h3>
              <p>Gayatri,1481, Near Ram Temple, Daji Peth, Solapur, Maharashtra 413005</p>
            </div>
          </div>

          <div class="col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="info-box">
              <i class="bx bx-envelope"></i>
              <h3>Email Us</h3>
              <p>eurotechpaints.solapur@gmail.com</p>
            </div>
          </div>

          <div class="col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="info-box ">
              <i class="bx bx-phone-call"></i>
              <h3>Call Us</h3>
              <p>+91 9370 722 875</p>
            </div>
          </div>

          <div class="col-lg-12" data-aos="fade-up" data-aos-delay="300">
            <form id="create" method="post" onsubmit="return contactus('create'); return false" class="php-email-form">
              <div class="form-row">
                <div class="col-lg-6 form-group">
                  <input type="text" id="cname" class="form-control" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <!-- <div class="validate"></div> -->
                </div>
                <div class="col-lg-6 form-group">
                  <input type="email" class="form-control" id="cemail" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <!-- <div class="validate"></div> -->
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control"  id="csubject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <!-- <div class="validate"></div> -->
              </div>
              <div class="form-group">
                <textarea class="form-control" id="cmessage" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <!-- <div class="validate"></div> -->
              </div>
              <!-- <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div> -->
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Us Section -->

  </main><!-- End #main -->

  <footer class="site-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <h6>About us</h6>
        <p>Copyright &copy; <?php echo date("Y");?>  All Rights Reserved by
        <a href="index.php">EuroTech</a>.
      </div>

      <div class="col-xs-6 col-md-3">
        <h6>Categories</h6>
        <ul class="footer-links">
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#products">Products</a></li>
          <li><a href="index.php#contact">Contact</a></li>
        </ul>
      </div>

      <div class="col-xs-6 col-md-3">
        <h6>Quick Links</h6>
        <ul class="footer-links">
          <li><a href="login.php">Login</a></li>
          <li><a href="index.php#faq">FAQ</a></li>
          <li><a href="index.php#about">AboutUs</a></li>
        </ul>
      </div>

    </div>
    <hr>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6 col-xs-12">
      <p class="copyright-text">Developed & Maintained By
        <a href="https://www.balsamleti.wordpress.com">Balchandra Samleti</a>.
      </p>
      </div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <ul class="social-icons">
          <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
          <!-- <li><a class="youtube" href="https://www.youtube.com/channel/UC8gl7BNdhxFbjZ9DF6b_Mmg"><i class="fa fa-youtube-play"></i></a></li> -->
          <li><a class="instagram" href="https://www.instagram.com/euro_tech_paints/"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>

</footer>

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
  <div class="loader">
    <div class="dot"></div>
    <div class="dot"></div>
    <div class="dot"></div>
    <div class="dot"></div>
    <div class="dot"></div>
  </div>
    <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="assets/js/contactus.js"></script>


</body>

</html>