<html>

<head>
    <meta charset="UTF-8">
    <title>EuroTech Paints</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/login.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        body {
            background-color: #F2F2F2;
        }
    </style>
</head>

<body>
    <div id="container-login">
        <div id="title">
            <i class="material-icons lock">lock</i> Admin Login
        </div>

        <form id="login" onsubmit="return authenticate('login'); return false;">
            <div class="input">
                <div class="input-addon">
                    <i class="material-icons">person</i>
                </div>
                <input name="useremail" type="email" placeholder="Username" required class="validate" autocomplete="off">
            </div>

            <div class="clearfix"></div>

            <div class="input">
                <div class="input-addon">
                    <i class="material-icons">vpn_key</i>
                </div>
                <input name="userpassword" class="validate" type="password" required autocomplete="off" placeholder="Password">
            </div>

            <br><br>

            <!-- <div class="remember-me">
                <input type="checkbox">
                <span style="color: #757575">Remember Me</span>
            </div> -->

            <input type="submit" value="Log In" /><br><br>
            <label style="color:red; font-weight:bold" id="message"></label>
        </form>

      
      

        <div class="register">
            <!-- <span style="color: #657575">Don't have an account yet?</span>
            <a href="#"><button id="register-link">Register here</button></a> -->
            <div class="forgot-password">
            <a href="#">Forgot your password?</a>
            
        </div>
        <p style="color:#000000">Euro Tech Paints, Solapur</p>
        <p>Restricted to Users</p>
        </div>
    </div>

    <script type="text/javascript" src="assets/js/login.js"></script>
</body>

</html>
