-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2020 at 09:10 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_eurotech`
--

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `entry_no` int(12) NOT NULL,
  `cust_email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `cust_subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cust_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sys_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`entry_no`, `cust_email`, `cust_name`, `cust_subject`, `cust_message`, `sys_date`) VALUES
(8, 'balsalsamleti@gmail.com', 'Balchandra Samleti', 'ushguke', 'kefgrnkgurdhukgr', '2020-10-31 16:21:49'),
(9, 'balsalsamleti@gmail.com', 'Balchandra Samleti', 'ushguke', 'kefgrnkgurdhukgr', '2020-10-31 16:21:49'),
(10, 'kjds@dmbf.dsg', 'mdbf', 'balasjfkb', 'jkdfnkjdg', '2020-10-31 16:22:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `sys_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `userpassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_verified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `sys_date`, `first_name`, `last_name`, `email`, `userpassword`, `is_verified`) VALUES
(1, '2020-10-31 14:35:29', 'Balchandra', 'Samleti', 'balsamleti@gmail.com', '$2y$10$dz1/H3FDI7aKGokRp4/PDO2DCeXbFCfxrzJjzwQdGdkVccsQvoiOS', 1),
(2, '2020-10-31 14:35:29', 'Aditya', 'Kondabattini', 'adityakondabatini@gmail.com', '$2y$10$buwmSab1llykJMfDr9h7ceT0OoLh2hS25XTTBthtqH8/tO8SotitW', NULL),
(3, '2020-10-31 17:04:29', 'Sheela', 'Jadhav', 'jadhavsheela144@gmail.com', '$2y$10$WGTLX9ZJ8IhdasmMvXuXRek6F0rIVXMN3nzAy7q2XaL68.v/ein1O', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`entry_no`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `entry_no` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
