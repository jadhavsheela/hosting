<html>

<head>
    <meta charset="UTF-8">
    <title>EuroTech Paints</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/login.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        body {
            background-color: #F2F2F2;
        }
    </style>
</head>

<body>

    <div id="container-register">
        
        <div id="title">
            <i class="material-icons lock">lock</i> Register as Admin
        </div>

        <form id="register" onsubmit="return authenticate('register'); return false">

            <div class="input">
                <div class="input-addon">
                    <i class="material-icons">person</i>
                </div>
                <input name="firstName"  placeholder="First Name" type="text" required class="validate" autocomplete="off">
            </div>

            <div class="input">
                <div class="input-addon">
                    <i class="material-icons">person</i>
                </div>
                <input name="lastName" placeholder="Last Name" type="text" required class="validate" autocomplete="off">
            </div>

            <div class="clearfix"></div>

            <div class="input">
                <div class="input-addon">
                    <i class="material-icons">mail</i>
                </div>
                <input name="useremail" placeholder="Email ID" type="email" required class="validate" autocomplete="off">
            </div>

            <div class="clearfix"></div>

            <div class="input">
                <div class="input-addon">
                    <i class="material-icons">vpn_key</i>
                </div>
                <input name="userpassword" placeholder="Password" type="password" required class="validate" autocomplete="off">
            </div>

            <div class="input">
                <div class="input-addon">
                    <i class="material-icons">vpn_key</i>
                </div>
                <input name="userpassword2" placeholder="Password" type="password" required class="validate" autocomplete="off">
            </div>

            <div class="remember-me">
                <input type="checkbox">
                <span style="color: #757575">I accept Terms of Service</span>
            </div>

           
            <input type="submit" value="Register" />
            <label style="color:red; font-weight:bold" id="message"></label>
            <p style=" text-size:10px; color: #757575">Contact DBA to Verify</p>
        </form>
<!-- 
        <div class="privacy">
            <a href="#">Privacy Policy</a>
        </div> -->

     
    </div>
</body>

<script type="text/javascript" src="assets/js/login.js"></script>

</html>
