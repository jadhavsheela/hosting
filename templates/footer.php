
<footer class="site-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <h6>About us</h6>
        <p>Copyright &copy; <?php echo date("Y");?>  All Rights Reserved by
        <a href="index.php">EuroTech</a>.
      </div>

      <div class="col-xs-6 col-md-3">
        <h6>Categories</h6>
        <ul class="footer-links">
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#products">Products</a></li>
          <li><a href="index.php#contact">Contact</a></li>
        </ul>
      </div>

      <div class="col-xs-6 col-md-3">
        <h6>Quick Links</h6>
        <ul class="footer-links">
          <li><a href="">Login</a></li>
          <li><a href="index.php#faq">FAQ</a></li>
          <li><a href="index.php#about">AboutUs</a></li>
        </ul>
      </div>

    </div>
    <hr>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6 col-xs-12">
      <p class="copyright-text">Developed & Maintained By
        <a href="https://www.balsamleti.wordpress.com">Balchandra Samleti</a>.
      </p>
      </div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <ul class="social-icons">
          <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
          <!-- <li><a class="youtube" href="https://www.youtube.com/channel/UC8gl7BNdhxFbjZ9DF6b_Mmg"><i class="fa fa-youtube-play"></i></a></li> -->
          <li><a class="instagram" href="https://www.instagram.com/euro_tech_paints/"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>

</footer>
