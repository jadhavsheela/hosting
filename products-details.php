<!DOCTYPE html>
<html lang="en">
<head>
<!-- Header Includes  -->
<meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>EuroTech Paints</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/footer.css">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- Preloader -->
  <link rel="stylesheet" href="assets/css/preloader.css">
<!-- Google Anaytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-180168407-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-180168407-1');
</script>

</head>

<body>
<style>
  .text{
    font-size:14px;
  }
</style>
  
  
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:eurotechpaints.solapur@gmail.com">eurotechpaints.solapur@gmail.com</a>
        <i class="icofont-phone"></i><a href="tel:+919370722875">+91 9370 722 875</a>
      </div>
      <div class="social-links float-right">
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="https://www.instagram.com/euro_tech_paints/" target="_blank" class="instagram"><i class="icofont-instagram"></i></a>
      </div>
    </div>
  </section>
 
 
 <!-- ======= Header ======= -->
 <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>Mamba</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="index.php"><img src="./assets/img/logo1.png" alt="eurotech_logo" class="img-fluid"><span class="text" style=" margin-left: 25px; margin-top: 10px; font-size: 25px;"><b>EURO TECH PAINTS</b></span></a></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="index.php#about">About Us</a></li>
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#products">Products</a></li>
          <!-- <li><a href="#team">Team</a></li> -->
          <!-- <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li> -->
          <li><a href="index.php#contact">Contact Us</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
  
  <main id="main">

    <!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Products Details</h2>
          <ol>
            <li><a href="index.php">Home</a></li>
            <li><a href="index.php#products">Products</a></li>
            <li>Portfolio Details</li>
          </ol>
        </div>

      </div>
    </section><!-- Breadcrumbs Section -->

    <!-- ======= Products Details Section ======= -->
    <section class="portfolio-details">
      <div class="container">

        <div class="portfolio-details-container">

          <div class="owl-carousel portfolio-details-carousel">
            <img src="./assets/img/1.JPG" class="img-fluid" alt="">
            <img src="./assets/img/2.JPG" class="img-fluid" alt="">
            <img src="./assets/img/3.JPG" class="img-fluid" alt="">
          </div>

          <div class="portfolio-info">
            <h3>Product Name</h3>
            <ul>
              <li><strong>Category</strong>: Web design</li>
              <li><strong>Specifications</strong>: 1kg / 5kg / 25kg</li>
              <li><strong>MRP</strong>: ₹ 235 </li>
            </ul>
          </div>
        </div>

        <div class="portfolio-description">
          <h2>Details</h2>
          <p>
          Waterproof Cement Paint With Titanium is manufactured by using White cement, Pigments, Titanium Dioxide and Chemicals etc. Its formulation pertains to easy mixing., longer pot life with smooth brushability and extra durability and longer life of point. It gives a protective coating to the surface against moisture, salinity and organic growth.
          </p>
        </div>
      </div>
    </section><!-- End Products Details Section -->

  </main><!-- End #main -->

  
<footer class="site-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <h6>About us</h6>
        <p>Copyright &copy; <?php echo date("Y");?>  All Rights Reserved by
        <a href="index.php">EuroTech</a>.
      </div>

      <div class="col-xs-6 col-md-3">
        <h6>Categories</h6>
        <ul class="footer-links">
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#products">Products</a></li>
          <li><a href="index.php#contact">Contact</a></li>
        </ul>
      </div>

      <div class="col-xs-6 col-md-3">
        <h6>Quick Links</h6>
        <ul class="footer-links">
          <li><a href="">Login</a></li>
          <li><a href="index.php#faq">FAQ</a></li>
          <li><a href="index.php#about">AboutUs</a></li>
        </ul>
      </div>

    </div>
    <hr>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6 col-xs-12">
      <p class="copyright-text">Developed & Maintained By
        <a href="https://www.balsamleti.wordpress.com">Balchandra Samleti</a>.
      </p>
      </div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <ul class="social-icons">
          <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
          <!-- <li><a class="youtube" href="https://www.youtube.com/channel/UC8gl7BNdhxFbjZ9DF6b_Mmg"><i class="fa fa-youtube-play"></i></a></li> -->
          <li><a class="instagram" href="https://www.instagram.com/euro_tech_paints/"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>

</footer>

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
  <div class="loader">
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
</div>
    <!-- Vendor JS Files -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>


</body>

</html>